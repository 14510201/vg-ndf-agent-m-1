#include "AppException.hpp"

AppException::AppException(const std::string& msg) : message(msg) {
}

const char* AppException::what() const noexcept {
    return message.c_str();
}
