#include <cstring>
#include <iostream>
#include <thread>
#include <algorithm>
#include <boost/thread.hpp>
#include <boost/format.hpp>
#include "FskModulator.hpp"
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <ifaddrs.h>
#include "ConfigSingleton.hpp"
#include "AppException.hpp"

using namespace std;

std::string FskModulator::CFG_GFSK_50KBPS_ALT = "CFG_GFSK_50KBPS_ALT";
std::string FskModulator::CFG_GFSK_50KBPS = "CFG_GFSK_50KBPS";

FskModulator::FskModulator(){
	this->networkUtils = new QLNetworkUtils();
};

FskModulator::~FskModulator(){
	delete networkUtils;
};

void FskModulator::_ServiceAttach(user_api::SvcFskModemConnectionUserApi &userConnect)
{
	char versionStr[32];
	char connectionVersionStr[32];

	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "FskModulator Initializing..." << std::endl;
	}

	while (userConnect.Attach() < 0)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

	// continually ping the service, wait for reply.
	bool ready = false;
	user_api::SvcFskModemCommand_t cmd{user_api::SvcFskModemCommand_t(user_api::CmdPing_t())};
	user_api::SvcFskModemResponse_t resp;
	do
	{
		userConnect.SendCommand(cmd);
		if (userConnect.ReceiveResponse(resp) == 0)
		{
			ready = (resp.Type() == user_api::ResponseType_t::Ready);
		}
	} while (!ready);
	cout << "Attached to SvcFskModem..." << endl;
	// Show versions
	{
		std::lock_guard<std::mutex> guard(mutCout);
		std::cout << "Service Vers: v" << this->pUserConnect.GetVersion()
				  << "  Connection Vers: v" << this->pUserConnect.GetConnectionVersion() << endl;
	}

};

void FskModulator::_SetConfiguration(std::string cfgName)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetConfiguration_t()) };
	user_api::CmdSetConfiguration_t& cmdConfig = cmd.setConfiguration;
	cmdConfig.config.Name(cfgName);
	cmdConfig.config.mod.pulseShapingBt = 1.0;
	this->pUserConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (this->pUserConnect.ReceiveResponse(resp) != 0);
	std::cout << "Configuration Set: " << cfgName << endl;
}

void FskModulator::Initialize()
{
	ConfigService* config = ConfigService::getInstance(); 
	this->interfaceName = config->getStrValue(ConfigService::INTERFACE_NAME);
	this->vlgwInterfaceIP = this->networkUtils->GetIPAddress(interfaceName);
	this->rpdMulticastIP = config->getStrValue(ConfigService::RPD_MULTICAST_IP);
	std::string rpdIp = config->getStrValue(ConfigService::RPD_IP);
	
	if (this->interfaceName.length() == 0 || this->vlgwInterfaceIP.length() == 0) {
		std::cerr << "IP Address not found for interface: "+ this->interfaceName << endl;
		throw new AppException("IP Address not found for interface: "+ this->interfaceName);
	} else {
		cout << "Source ip address: " << this->vlgwInterfaceIP << " for interface: "+ this->interfaceName << endl;
	}

	cout << "Getting MAC for src ipAddr: " << this->vlgwInterfaceIP << " dst ipAddr (RPD ip Address for getting MAC): " << rpdIp << endl;
	pcpp::MacAddress gwMac = this->networkUtils->GetMyMac(this->vlgwInterfaceIP);
	pcpp::MacAddress rpdMac = this->networkUtils->GetMacForIP(this->vlgwInterfaceIP, rpdIp);
	
	this->networkUtils->SetDstAddr(this->rpdMulticastIP);
	this->networkUtils->SetSrcAddr(this->vlgwInterfaceIP);
	this->networkUtils->setDstMac(rpdMac);
	this->networkUtils->setSrcMac(gwMac);
	this->networkUtils->SetSessionID(config->getInt32Value(ConfigService::SESSION_ID));

	cout << "RPD MAC: " << rpdMac.toString() 
		<< " GW MAC: " << gwMac.toString() 
		<< endl;
	// attach to the modem service
	_ServiceAttach(this->pUserConnect);
	//_SetConfiguration(FskModulator::CFG_GFSK_50KBPS);
	//this->DiagnosticsEnable(this->pUserConnect, true);
}

void FskModulator::StartModulatedPktListenerThread()
{
	uint8_t frame[1400];
	int frameLength;
	int frameCount = 0;

	try {
		//this->networkUtils->JoinMulticastGroup(this->rpdMulticastIP, this->vlgwInterfaceIP);
		this->networkUtils->CreatePcapLiveDevice(this->interfaceName);
		this->stopListenForModIq = false;
	} catch(const AppException* e){
		cerr << "Failed to start thread. While trying to open multicast error: " << e->what() << endl;
		this->stopListenForModIq = true;
		delete e;
	}
	
	while (!stopListenForModIq)
	{
		if ((frameLength = pUserConnect.ReceiveModIqFrame((void *)frame, 1400)) > 0)
		{
			std::lock_guard<std::mutex> guard(mutCout);
			//std::cout << "IQ frame received (Len " << std::dec << frameLength << ") #" << frameCount << std::endl;
			frameCount++;
			this->networkUtils->SendPacket(frame, frameLength);
		}
	}
	std::lock_guard<std::mutex> guard(mutCout);
	std::cout << "IQ frame count: " << std::dec << frameCount << std::endl
			  << std::endl;
	cout << "ModulatedPktListener thread stopping" << endl;
	//this->networkUtils->CloseSocket();
	this->networkUtils->CloseDevice();
};

void FskModulator::DiagnosticsEnable(user_api::SvcFskModemConnectionUserApi& userConnect, bool enable)
{
	// send cmd, await completion response
	user_api::SvcFskModemCommand_t cmd{ user_api::SvcFskModemCommand_t(user_api::CmdSetDiagnostics_t()) };
	user_api::CmdSetDiagnostics_t& cmdDiags = cmd.setDiagnostics;
	cmdDiags.demod.enable = enable;
	cmdDiags.mod.enable = enable;
	userConnect.SendCommand(cmd);
	user_api::SvcFskModemResponse_t resp;
	while (userConnect.ReceiveResponse(resp) != 0);

	if (enable)
	{
		std::cout << "Diagnostics Enabled" << std::endl;
	}
	else
	{
		std::cout << "Diagnostics Disabled" << std::endl;
	}
}

void FskModulator::StopListener()
{
	std::cout << "Stopping ModulatedPktListener thread" << std::endl;
	this->DiagnosticsEnable(this->pUserConnect, false);
	this->stopListenForModIq = true;
}

user_api::SvcFskModemConnectionUserApi *FskModulator::GetHandle()
{
	return &this->pUserConnect;
};
