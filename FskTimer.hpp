#include <iostream>
#include <chrono>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/bind_executor.hpp>
#include "SvcFskModemConnectionUserApi.hpp"

namespace asio = boost::asio;
using namespace svc_fsk_modem;
class Timer {

public:
    Timer(asio::thread_pool& pool, std::chrono::microseconds timeout,
          std::function<void(void *, size_t, uint8_t, user_api::SvcFskModemConnectionUserApi* pUserConnect)> callback,
          void* data, size_t length, uint8_t channel, user_api::SvcFskModemConnectionUserApi* pUserConnect)
          : m_timer(pool), 
            m_callback(callback), 
            m_data(data), 
            m_length(length), 
            m_channel(channel),
            pUserConnect(pUserConnect) {
        m_timer.expires_from_now(timeout);
        m_timer.async_wait(asio::bind_executor(pool.executor(), std::bind(&Timer::handleTimeout, this, std::placeholders::_1)));
    }

    void handleTimeout(const boost::system::error_code& ec) {
        if (!ec) {
            // Timer has expired, call the callback function with the data
            m_callback(m_data, m_length, m_channel, pUserConnect);
        }
    }

private:
    asio::steady_timer m_timer;
    std::function<void(void*, size_t, uint8_t, user_api::SvcFskModemConnectionUserApi*)> m_callback;
    user_api::SvcFskModemConnectionUserApi* pUserConnect;
    void* m_data;
    size_t m_length;
    uint8_t m_channel;
};