#include <iostream>
#include <cstring>
#include <chrono>
#include <boost/interprocess/exceptions.hpp>
#include <boost/format.hpp>
#include <signal.h>
#include <fstream>

#include <nlohmann/json.hpp>
#include <boost/program_options.hpp>
#include "NDFAgent.hpp"
#include "AppException.hpp"

using namespace std;

namespace po = boost::program_options;
using json = nlohmann::json;

//NdfDataCore*	ndfDataCore = NULL;
NdfAgent *agent = nullptr;

void SignalHandler(int signum)
{
    std::cout << "Interrupt signal (" << signum << ") received.\n";	
    if (agent != nullptr)
    {
        agent->Shutdown();
    }
}


int main(int argc, char *argv[])
{
    try
    {
        po::options_description desc("Options");
        ConfigService *config = ConfigService::getInstance();
        desc.add_options()
            ("help,h", "produce help message")("version,v", "print version string")
            ("interface,i", po::value<std::string>(), "interface to send on")
            ("rpd-multicast-ip,m", po::value<std::string>(), "Multicast IP address of the RPD") 
            ("rpd-ip,d", po::value<std::string>(), "Unicast IP address of the RPD")
            ("gateway-id,g", po::value<std::string>(), "Gateway ID") 
            ("session-id,s", po::value<std::string>(), "Session ID hex value")
            ("redis-ip,r", po::value<std::string>(), "Redis server address")
            ("redis-port,p", po::value<std::string>(), "Redis server port")
            ("redis-pwd,w", po::value<std::string>(), "Redis server password");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            return 1;
        }

        if (vm.count("version"))
        {
            std::cout << "Version 1.0.0\n";
        }

        if (!(vm.count("interface") 
            && vm.count("session-id") 
            && vm.count("rpd-ip")
            && vm.count("rpd-multicast-ip") 
            && vm.count("gateway-id")))
        {
            std::cout << "Options: interface , rpd-ip, rpd-multicast-ip, gateway-id and session-id are required\n";
            std::cout << desc << "\n";
            return 1;
        }

        std::string sessionId = vm["session-id"].as<std::string>();

        if (sessionId.find("0x") == 0 || sessionId.find("0X") == 0)
        {
            sessionId.erase(0, 2); // Remove the '0x' prefix
        }
        uint32_t sessionIdLong = 0;
        try
        {
            sessionIdLong = std::stoul(sessionId, nullptr, 16);
            config->setValue("session-id", sessionIdLong);
            std::cout << "The session ID is: " << sessionIdLong << " (0x" << std::hex << sessionIdLong << ")" << std::endl;
        }
        catch (const std::invalid_argument &ia)
        {
            std::cerr << "Invalid session ID: " << ia.what() << std::endl;
            std::cout << desc << "\n";
            return 1;
        }
        catch (const std::out_of_range &oor)
        {
            std::cerr << "Session ID of Range error: " << oor.what() << std::endl;
            std::cout << desc << "\n";
            return 1;
        }

        if(!(vm.count("redis-port") && vm.count("redis-ip")))
        {
            std::cout << "Options redis-port and redis-ip are required\n";
            std::cout << desc << "\n";
            return 1;
        }

        std::string interface = vm["interface"].as<std::string>();
        std::string multicastIP = vm["rpd-multicast-ip"].as<std::string>();
        std::string rpdIP = vm["rpd-ip"].as<std::string>();
        std::string redisIp = vm["redis-ip"].as<std::string>();
        std::string portStr = vm["redis-port"].as<std::string>();
        std::string gwId = vm["gateway-id"].as<std::string>();
        
        config->setValue(ConfigService::REDIS_IP,redisIp);
        config->setValue(ConfigService::REDIS_PORT,portStr);
        config->setValue(ConfigService::GATEWAY_ID,gwId);
        config->setValue(ConfigService::INTERFACE_NAME,interface);
        config->setValue(ConfigService::RPD_MULTICAST_IP,multicastIP);
        config->setValue(ConfigService::RPD_IP,rpdIP);

        agent = new NdfAgent(config);

        signal(SIGINT, SignalHandler);

        try {
            agent->Initialize();
            agent->Start();
        } catch (const AppException* e){
            std::cout << "Caught an AppException: " << e->what() << std::endl;
            delete e;
        }
    }

    catch (const po::error &ex)
    {
        std::cerr << ex.what() << '\n';
        return 1;
    }
}

NdfAgent::NdfAgent(ConfigService* config){
    this->config = config;
}

NdfAgent::~NdfAgent(){

}

void NdfAgent::Initialize(){
    
    std::string gwId = config->getStrValue(ConfigService::GATEWAY_ID);
    std::string redisIp = config->getStrValue(ConfigService::REDIS_IP);
    std::string redisPortStr = config->getStrValue(ConfigService::REDIS_PORT);

    std::string redisFromQueueName = "ndf_" + gwId;
    cout << "Initializing redis client - server: " << redisIp << ":" << redisPortStr << " queue: "<< redisFromQueueName << endl;
    int redisPort = std::stoi( redisPortStr );

    this->fskModulator = new FskModulator();
    this->fskModulator->Initialize();
    this->redisClient = new RedisClient(
        redisIp, 
        redisPort, 
        redisFromQueueName, 
        this->fskModulator->GetHandle());
}

void NdfAgent::Start(){
    if(!running || !stop) {
        running = true;
        stop = false;

        std::thread fskModulatorThread(&FskModulator::StartModulatedPktListenerThread, this->fskModulator);
        std::thread redisClientThread(&RedisClient::StartListener, this->redisClient);
        std::thread timerThread(&RedisClient::StartAsyncSender, this->redisClient);

        fskModulatorThread.join();
        redisClientThread.join();
        timerThread.join();

    } else {
        cout << "Failed to start NdfAgent";
    }
}

void NdfAgent::Shutdown(){
    cout << "Interrupt received - stopping program" << endl;
    this->stop = true;
    this->redisClient->StopListener();
    this->fskModulator->StopListener();
}