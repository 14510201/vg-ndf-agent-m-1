#ifndef NDF_REDIS_CLIENT_H
#define NDF_REDIS_CLIENT_H

#include <iostream>
#include <sw/redis++/redis.h>
#include <sw/redis++/redis_cluster.h>
#include "SvcFskModemConnectionUserApi.hpp"
#include <boost/asio/io_context.hpp>
#include <chrono>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/bind_executor.hpp>

using namespace svc_fsk_modem;
namespace asio = boost::asio;

struct DataWithLength {
    void* data;
    size_t length;
    uint8_t channel;
};

bool compareUint32(const uint32_t& u1, const uint32_t& u2);

class RedisClient
{

    sw::redis::Redis redisClient;
    bool isRunning = false;
    bool stop = false;
    std::string queueName;
    user_api::SvcFskModemConnectionUserApi* pUserConnect;
    
    std::map<uint32_t, DataWithLength, decltype(compareUint32)*> timerMap;

public:
    RedisClient() = delete;
    RedisClient(
        const std::string& ip, 
        int port, 
        const std::string& queueName, 
        user_api::SvcFskModemConnectionUserApi* pUserConnect);
        
    ~RedisClient();

    std::vector<unsigned char> base64_decode(const std::string &base64_string);

    void StartListener();
    void StartAsyncSender();
    void StopListener();
};

#endif // NDF_REDIS_CLIENT_H
