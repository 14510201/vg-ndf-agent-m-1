#ifndef NDF_AGENT_H
#define NDF_AGENT_H

#include <string>
#include "ConfigSingleton.hpp"
#include "RedisClient.hpp"
#include "FskModulator.hpp"
#include <thread>

class NdfAgent {
    private:
        ConfigService* config;
        RedisClient* redisClient;
        FskModulator* fskModulator;
        bool running = false;
        bool stop = false;

    public:
        NdfAgent(ConfigService* config);
        ~NdfAgent();
        void Initialize();
        void Start();
        void Shutdown();
};

#endif // NDF_AGENT_H