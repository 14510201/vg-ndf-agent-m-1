

#include <iostream>
#include "NetworkUtils.hpp"

#ifndef PCPP_ARP_REPLY
#define PCPP_ARP_REPLY 2
#endif
#include <SystemUtils.h>
#include "AppException.hpp"
#include <unistd.h>
#include <ifaddrs.h>
#include "IPv4Layer.h"
#include "PayloadLayer.h"
#include "Packet.h"
#include "PcapFileDevice.h"
#include <stdint.h>
#include <endian.h>
#include "MacAddress.h"

using namespace std;

#include <iostream>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <array>
#include <regex>

std::string execCommand(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::string getMACAddress(const std::string& ipAddress) {
    // Ensure the IP address is in the ARP cache by pinging it
    std::string pingCmd = "ping -c 1 " + ipAddress;
    execCommand(pingCmd.c_str());

    // Attempt to retrieve the MAC address from the ARP cache
    std::string arpCmd = "arp -n " + ipAddress;
    std::string arpOutput = execCommand(arpCmd.c_str());

    // Parse the ARP command output
    std::regex macRegex("(([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))");
    std::smatch macMatch;
    if (std::regex_search(arpOutput, macMatch, macRegex)) {
        return macMatch.str(0);
    }

    return "MAC Address not found";
}

QLNetworkUtils::QLNetworkUtils()
{
    this->sequenceNumber = 0;
}

QLNetworkUtils::~QLNetworkUtils() {}

struct CallbackState
{
    pcpp::IPv4Address targetIp;
    pcpp::MacAddress *targetMac;
    bool *stopCapture;
};


pcpp::MacAddress QLNetworkUtils::GetMacForIP(std::string interfaceIpAddr, std::string ipAddr)
{
    pcpp::IPv4Address targetIp(ipAddr); // Target IP address

    pcpp::PcapLiveDevice *dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByIp(interfaceIpAddr.c_str());
    if (dev == nullptr || !dev->open())
    {
        std::cerr << "Cannot find or open device for IP " << interfaceIpAddr << std::endl;
    }

    //pcpp::MacAddress targetMac = this->waitForArpReply(dev, targetIp);
    std::string mac = getMACAddress(ipAddr);
    pcpp::MacAddress targetMac = pcpp::MacAddress(mac);
    if (!targetMac.isValid())
    {
        std::cout << "No ARP reply received" << std::endl;
    }
    else
    {
        std::cout << "MAC address for IP " << targetIp.toString() << " is " << targetMac.toString() << std::endl;
    }

    dev->close();
    return targetMac;
}

pcpp::MacAddress QLNetworkUtils::GetMyMac(std::string interfaceIpAddr)
{
    pcpp::IPv4Address targetIp(interfaceIpAddr);
    pcpp::PcapLiveDevice *dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByIp(targetIp.toString().c_str());
    return dev->getMacAddress();
}

void QLNetworkUtils::setDstMac(pcpp::MacAddress mac)
{
    this->dstMac = mac;
}

void QLNetworkUtils::setSrcMac(pcpp::MacAddress mac)
{
    this->srcMac = mac;
}

std::string QLNetworkUtils::GetIPAddress(const std::string &interfaceName)
{
    struct ifaddrs *ifaddr, *ifa;
    char addrStr[INET_ADDRSTRLEN];

    if (getifaddrs(&ifaddr) == -1)
    {
        perror("getifaddrs");
        return "";
    }

    for (ifa = ifaddr; ifa != nullptr; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == nullptr)
            continue;

        if (ifa->ifa_addr->sa_family == AF_INET && strcmp(ifa->ifa_name, interfaceName.c_str()) == 0)
        {
            // Found a matching interface with an IPv4 address
            void *tmpAddrPtr = &((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
            inet_ntop(AF_INET, tmpAddrPtr, addrStr, INET_ADDRSTRLEN);
            freeifaddrs(ifaddr);
            return std::string(addrStr);
        }
    }

    freeifaddrs(ifaddr);
    return ""; // No matching interface found
}

void QLNetworkUtils::SetSrcAddr(std::string srcIp)
{
    this->srcAddr = pcpp::IPv4Address(srcIp);
}
void QLNetworkUtils::SetDstAddr(std::string dstIp)
{
    this->dstAddr = pcpp::IPv4Address(dstIp);
}

bool QLNetworkUtils::CreatePcapLiveDevice(std::string interfaceName)
{
    this->dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(interfaceName);
    if (dev == nullptr || !dev->open())
    {
        std::cerr << "Cannot find or open device " << interfaceName << std::endl;
        return false;
    }
    return true;
}

bool QLNetworkUtils::SendPacket(uint8_t *frame, int length)
{
    pcpp::EthLayer ethLayer(srcMac, dstMac, PCPP_ETHERTYPE_IP);
    pcpp::IPv4Layer ipLayer(srcAddr, dstAddr);
    uint8_t hdr[8];
    this->sequenceNumber++;

    // Assuming little-endian architecture (adjust if big-endian)
//#if __BYTE_ORDER == __LITTLE_ENDIAN
    //memcpy(hdr, (uint8_t *)&sessionId, 4);
    //memcpy(hdr+4,(uint8_t *)&sequenceNumber, 4);
//#elif __BYTE_ORDER == __BIG_ENDIAN
    // Reverse bytes explicitly for big-endian
    uint32_t reversedSessionId = htobe32(sessionId);
    uint32_t reversedSequenceNumber = htobe32(sequenceNumber);

    memcpy(hdr, (uint8_t *)&reversedSessionId, 4);
    memcpy(hdr+4, (uint8_t *)&reversedSequenceNumber, 4);
// #else
//     #error "Unsupported endianness"
// #endif

    pcpp::PayloadLayer l2tpv3Header(hdr, 8, true);

    ipLayer.getIPv4Header()->protocol = 115;

    uint32_t *uint32_ptr = (uint32_t *)frame;

    this->CorrectEndianness(uint32_ptr,length/4);

    pcpp::PayloadLayer payloadLayer(frame, length, true);

    // Craft the packet
    pcpp::Packet packet;
    packet.addLayer(&ethLayer);
    packet.addLayer(&ipLayer);
    packet.addLayer(&l2tpv3Header);
    packet.addLayer(&payloadLayer);

    packet.computeCalculateFields();

    if (!dev->sendPacket(&packet)) {
        std::cerr << "Failed to send packet" << std::endl;
        return false;
    }else {
        //std::cout << "Packet sent to " << this->dstAddr << std::endl;
    }
    return true;
}

void QLNetworkUtils::CloseDevice(){
    dev->close();
    cout << "Device closed" << endl;
}

void QLNetworkUtils::SetSessionID(uint32_t sessionId){
    this->sessionId = sessionId;
}

inline void QLNetworkUtils::CorrectEndianness(uint32_t* pValues, int count)
{
    // byteswap
    uint8_t* pValueAsBytes = (uint8_t*)pValues;
    for (int idx = 0; idx < count; idx++)
    {
        pValues[idx] = (pValueAsBytes[0] << 24) | (pValueAsBytes[1] << 16) | (pValueAsBytes[2] << 8) | (pValueAsBytes[3] << 0);
        pValueAsBytes += 4;
    }
}