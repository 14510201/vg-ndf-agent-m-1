# VLGW NDF Agent

How to build the project

    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/lib
    cmake -S . -B build
    cmake --build build --target=install


Payload:
    {
        "join_eui" : "0000000000000000",
        "dev_eui": "0000000000000006",
        "app_key":"2b7e151628aed2a6abf7158809cf4f3c",
        "freq": 868.1
    }

Wireshark
    ip.addr == 112.97.0.42
    

## Getting started

This application uses RAW sockets, therefore it requires administrative privileges and must be run as root.
It also needs to bind to a specific interface, therefore the interface IP needs to be specified.
When this application will run within kubernetes environments, there will be some special considerations for it.



