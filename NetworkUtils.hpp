#ifndef NETWORK_UTILS_H
#define NETWORK_UTILS_H

#include <string>
#include "PcapLiveDeviceList.h"
#include "EthLayer.h"
#include "PcapFileDevice.h"
#include "Packet.h"
#include "ArpLayer.h"
#include "Packet.h"
#include <arpa/inet.h>

class QLNetworkUtils {

private:
    pcpp::MacAddress srcMac;
    pcpp::MacAddress dstMac;
    pcpp::IPv4Address srcAddr;
    pcpp::IPv4Address dstAddr;

    uint32_t sessionId;
    u_int32_t sequenceNumber;
    
    pcpp::PcapLiveDevice* dev;

    int sockfd;
    
public:
    QLNetworkUtils();
    ~QLNetworkUtils();
    std::string GetIPAddress(const std::string& interfaceName);
    pcpp::MacAddress GetMacForIP(std::string interfaceIpAddr, std::string ipAddr); 
    pcpp::MacAddress GetMyMac(std::string interfaceIpAddr); 

    static inline void CorrectEndianness(uint32_t* pValues, int count);
    
    //void JoinMulticastGroup(const std::string& multicastIP, const std::string& interfaceIP);
    bool CreatePcapLiveDevice(std::string interfaceName);

    //void CloseSocket();
    void CloseDevice();
    
    void setSrcMac(pcpp::MacAddress mac);
    void setDstMac(pcpp::MacAddress mac);
    void SetSrcAddr(std::string srcIp);
    void SetDstAddr(std::string dstIp);
    void SetSessionID(uint32_t sessionId);
    bool SendPacket(uint8_t *frame, int length);

};

#endif