#ifndef CONFIG_SINGLETON_H
#define CONFIG_SINGLETON_H

#include <string>
#include <map>
#include <mutex>
#include <variant>
#include <variant>

class ConfigService {
    struct NDFConfig
    {
        std::string sourceIP;
        std::string destIP;
        std::string sessionID;
        std::string gwID;
        std::string interfaceName;
    };
private:
    // Private static instance of the class
    static ConfigService* instance;

    // Private constructor for preventing external instantiation
    ConfigService() {}

    // Container for storing configuration settings
    std::map<std::string, std::variant<uint32_t, std::string>> configSettings;

    // Mutex for thread-safe instance creation
    static std::mutex mutex;

public:
    // Delete copy constructor and assignment operator
    ConfigService(const ConfigService&) = delete;
    ConfigService& operator=(const ConfigService&) = delete;

    // Static method for accessing the class instance
    static ConfigService* getInstance();

    // Method for setting a configuration value (overloaded for int and std::string)
    void setValue(const std::string& key, const std::variant<uint32_t, std::string>& value);
    std::string getStrValue(const std::string& key);
    uint32_t getInt32Value(const std::string& key);
    // Method for getting a configuration value
    std::variant<uint32_t, std::string> getValue(const std::string& key);

    NDFConfig ndfConfig;

    static std::string REDIS_IP;
    static std::string REDIS_PORT;
    static std::string SESSION_ID;
    static std::string GATEWAY_ID;
    static std::string RPD_MULTICAST_IP;
    static std::string RPD_IP;
    static std::string INTERFACE_NAME;
};

#endif // CONFIG_SINGLETON_H
