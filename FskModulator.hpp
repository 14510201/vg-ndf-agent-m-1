#include "SvcFskModemConnectionUserApi.hpp"
#include <mutex>

#include "NetworkUtils.hpp"

using namespace svc_fsk_modem;

class FskModulator {

private:
    std::mutex mutCout;
    user_api::SvcFskModemConnectionUserApi pUserConnect;
    
    bool stopListenForModIq = false;
    
    std::string rpdMulticastIP;
    std::string vlgwInterfaceIP;
    std::string interfaceName;

    int sockfd;
    QLNetworkUtils* networkUtils;

    void _ServiceAttach(user_api::SvcFskModemConnectionUserApi& userConnect);
    void _SetConfiguration(std::string cfgName);
    void DiagnosticsEnable(user_api::SvcFskModemConnectionUserApi& userConnect, bool enable);

public:
    FskModulator();
    virtual ~FskModulator();
    void Initialize();
    void StartModulatedPktListenerThread();
    void StopListener();
    user_api::SvcFskModemConnectionUserApi* GetHandle();

    static std::string CFG_GFSK_50KBPS;
    static std::string CFG_GFSK_50KBPS_ALT;
    
};
