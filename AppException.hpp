#ifndef APPEXCEPTION_H
#define APPEXCEPTION_H

#include <exception>
#include <string>

class AppException : public std::exception {
private:
    std::string message;
public:
    // Constructor with a message
    explicit AppException(const std::string& msg);

    // Override the what() method
    const char* what() const noexcept override;
};

#endif // APPEXCEPTION_H
