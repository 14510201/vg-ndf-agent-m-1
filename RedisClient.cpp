

#include "RedisClient.hpp"
#include <iomanip>
#include <openssl/aes.h>
#include <iostream>
#include <string>

using namespace svc_fsk_modem;

#include <vector>
#include <openssl/aes.h>
#include <utility>
#include <unistd.h>
#include <nlohmann/json.hpp>
#include <tuple>
#include <iostream>
#include <sstream>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include "FskTimer.hpp"
#include <shared_mutex>

std::shared_mutex timerMapMutex;


// Define a custom comparison function that compares two instances of uint32_t
bool compareUint32(const uint32_t& u1, const uint32_t& u2) {
    return u1 < u2;
}

void printHex(const unsigned char* data, size_t length) {
    for (size_t i = 0; i < length; i++) {
        std::cout << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(data[i]);
    }
    std::cout << std::endl;
}

std::vector<unsigned char> hexStringToBinary(const std::string &hex)
{
    std::vector<unsigned char> bytes;

    for (size_t i = 0; i < hex.length(); i += 2)
    {
        std::string byteString = hex.substr(i, 2);
        unsigned char byte = static_cast<unsigned char>(strtol(byteString.c_str(), nullptr, 16));
        bytes.push_back(byte);
    }

    return bytes;
}
int aes_ecb_encrypt(unsigned char *data, size_t data_len)
{

    std::string hexKey = "2b7e151628aed2a6abf7158809cf4f3c";
    std::vector<unsigned char> binaryKey = hexStringToBinary(hexKey);
    AES_KEY aesKey;
    if (AES_set_encrypt_key(binaryKey.data(), 128, &aesKey) < 0)
    {
        std::cerr << "AES_set_encrypt_key failed" << std::endl;
        return -1;
    }

    // Calculate padding length
    size_t padding_len = (AES_BLOCK_SIZE - (data_len % AES_BLOCK_SIZE)) % AES_BLOCK_SIZE;
    size_t padded_len = data_len + padding_len;

    // Prepare padded input
    std::vector<unsigned char> paddedData(padded_len);
    memcpy(paddedData.data(), data, data_len);
    if (padding_len > 0)
    {
        // Simple PKCS#5 padding for example purposes
        memset(paddedData.data() + data_len, padding_len, padding_len);
    }

    // Encrypt data
    std::vector<unsigned char> encryptedData(padded_len);
    for (size_t i = 0; i < padded_len; i += AES_BLOCK_SIZE)
    {
        AES_ecb_encrypt(paddedData.data() + i, encryptedData.data() + i, &aesKey, AES_ENCRYPT);
    }

    std::ostringstream ss;
    for (auto byte : encryptedData)
    {
        ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(byte);
    }
    std::cout << "Decrypted Message (Hex): " << ss.str() << std::endl;

    return 0;
}

RedisClient::RedisClient(const std::string &ip,
                         int port,
                         const std::string &queueName,
                         user_api::SvcFskModemConnectionUserApi *pUserConnect)
    : redisClient("tcp://" + ip + ":" + std::to_string(port)), timerMap(compareUint32)
{
    this->queueName = queueName;
    this->pUserConnect = pUserConnect;
}

RedisClient::~RedisClient()
{
}

void RedisClient::StartListener()
{
    isRunning = true;
    stop = false;
    int chanNum = 0;
    while (!stop)
    {
        sw::redis::OptionalStringPair opt_pair = redisClient.brpop(this->queueName, 1);
        
        if (opt_pair)
        {
            std::pair<std::string, std::string> value = *opt_pair;
            std::string loraPkt = value.second;
            std::string payload = std::get<1>(*opt_pair);


            // Parse the payload as JSON
            nlohmann::json j = nlohmann::json::parse(payload);
            std::cout << j << std::endl;

            std::cout << "Packet: " << j << std::endl;
            std::string base64_data = j["data"];
            uint8_t channel = j.contains("channel") ? j["channel"].get<uint8_t>() : 0;
            bool imme = j.contains("imme") ? j["imme"].get<bool>() : false;
            uint32_t tmst = j.contains("tmst") ? j["tmst"].get<uint32_t>() : 0;
            // Decode the base64 data
            std::vector<unsigned char> bytes_data = this->base64_decode(base64_data);

            // Get the length of the decoded bytes data
            size_t length = bytes_data.size();
            void *byteArray = const_cast<void *>(static_cast<const void *>(bytes_data.data()));
            printHex( static_cast<const unsigned char *>(byteArray),length);
            if (imme)
            {
                while (pUserConnect->SendModPacket((void *)byteArray, length, channel) < 0);
            }
            else
            {
                auto now_ms = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                uint32_t now_ms_uint32 = static_cast<uint32_t>(now_ms);
                std::cout << std::dec << "tmst: " << tmst << " now_ms_uint32: " << now_ms_uint32 << " diff: " << tmst - now_ms_uint32 << std::endl;
                if (tmst - now_ms_uint32 < 0)
                {
                    std::cout << "ERROR: The time window has expired" << std::endl;
                    while (pUserConnect->SendModPacket((void *)byteArray, length, channel) < 0)
                        ;
                }
                else
                {
                    DataWithLength _t_payload;
                    _t_payload.data = malloc(length);
                    memcpy(_t_payload.data, bytes_data.data(), length);
                    _t_payload.length = length;
                    _t_payload.channel = channel;
                    std::cout << "Adding pkt to timerMap tmst: " << tmst << std::endl;
                    std::scoped_lock<std::shared_mutex> lock(timerMapMutex);
                    this->timerMap[tmst] = _t_payload;
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    isRunning = false;
}

// void timer_callback(const unsigned char* byteArray, size_t length, uint8_t channel, user_api::SvcFskModemConnectionUserApi* pUserConnect){
//     std::cout << "INFO: Timer expired sendind packets" << std::endl;
//     while (pUserConnect->SendModPacket((void*)byteArray, length, channel) < 0);
// }

std::vector<unsigned char> RedisClient::base64_decode(const std::string &base64_string) {
    // Create a BIO for Base64 decoding
    BIO *b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    // Create a BIO for reading from the Base64 string
    BIO *bio = BIO_new_mem_buf(base64_string.c_str(), -1);
    BIO_push(b64, bio);

    // Create a memory buffer BIO for writing decoded data
    BIO *out = BIO_new(BIO_s_mem());

    // Decode the Base64 data
    unsigned char buffer[1024];
    int length;
    while ((length = BIO_read(b64, buffer, sizeof(buffer))) > 0) {
        BIO_write(out, buffer, length);
    }

    // Get the decoded data as a vector
    unsigned char *decoded_data;
    long decoded_length = BIO_get_mem_data(out, &decoded_data);
    std::vector<unsigned char> decoded_bytes(decoded_data, decoded_data + decoded_length);

    // Free the BIOs
    BIO_free_all(b64);

    return decoded_bytes;
}

void RedisClient::StartAsyncSender()
{
    std::cout << "RedisClient::StartAsyncSender: starting" << std::endl;
    while(!this->stop)
    {
        try
        {
            //std::cout << "RedisClient::StartAsyncSender: sleeping for 1ms" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            uint32_t tmst;
            DataWithLength d;
            {
                std::scoped_lock<std::shared_mutex> lock(timerMapMutex);
                if (this->timerMap.empty())
                {
                    //std::cout << "RedisClient::StartAsyncSender: timerMap is empty" << std::endl;
                    continue;
                }
                tmst = this->timerMap.begin()->first;
                d = this->timerMap.begin()->second;
                this->timerMap.erase(this->timerMap.begin());
            }
            auto now_ms = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
            uint32_t now_ms_uint32 = static_cast<uint32_t>(now_ms);
            std::cout << std::dec << "RedisClient::StartAsyncSender: tmst: " << tmst << " now_ms_uint32: " << now_ms_uint32 << " diff: " << tmst - now_ms_uint32 << std::endl;
            int tmst_diff = (tmst - now_ms_uint32); //4645273
            std::cout << "RedisClient::StartAsyncSender: tmst_diff2: " << tmst_diff << std::endl;
            if(tmst_diff < 0)
            {
                std::cout << "RedisClient::StartAsyncSender: sending packet immediately" << std::endl;
                while (pUserConnect->SendModPacket(d.data, d.length, 5) < 0);
            }else {
                int w = 4710000;
                int c = 0;
                //while(tmst_diff >  0 && c < 1){
                std::this_thread::sleep_for(std::chrono::microseconds(int(tmst_diff)));
                std::cout << "RedisClient::StartAsyncSender: sending packet [" << c++ << "] tmst_diff:" << tmst_diff << " w:" << w << std::endl;
                printHex( static_cast<const unsigned char *>(d.data), d.length);
                while (pUserConnect->SendModPacket(d.data, d.length, d.channel) < 0);
                tmst_diff -= w;
                //}
            }
            free(d.data);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Caught exception in RedisClient::StartAsyncSender: " << e.what() << std::endl;
        }
    }
}

void RedisClient::StopListener()
{
    stop = true;
}
